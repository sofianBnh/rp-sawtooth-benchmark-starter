# Benchmarking Sawtooth

This repository has the scripts used to test and deploy the Sawtooth Distributed Ledger Technology Platform. 
In it, one can find the automated deployment script for quickly having a network running on docker. The script
uses docker and docker-compose and generates a docker-compose file for each node. The script then uses them to
start the network of nodes.

Each node is composed of the following containers:

- Validator
- Rest API
- Settings Transaction Processor
- PoET Engine
- PoET Validator Registry Transaction Processor
- Benchmarking Transaction Processors:
    * Do Nothing TP
    * Key Value Store TP
    * CPU Heavy TP
    * Small Bank TP

**Disclaimer**: This is benchmark does not use some of the key optimization features of sawtooth such as batches and parallel processing of transactions. Better performance would be expected using these features. 

**Conventions**: In this project, the following conventions were followed:

- The ports used for the Rest API and the Validator are the default ones i.e. *8008* and *4004* respectively.
- The application events are based on the name of the benchmark: `event_name => benchmark_name '-' event`.
- The 'boot node' is the first node to be started. It is the one that is responsible for generating the genesis block and serves as a seed for the other nodes.

## Sources

For making this project, the following sources were used:

- [Domonit](https://github.com/eon01/DoMonit/tree/master/domonit): Monitoring Docker Container, was ported to python3 and had some bug fixes.
- [Sawtooth Docs](https://sawtooth.hyperledger.org/docs/core/releases/1.1.2/introduction.html): Official Documentation for the base of the docker-compose files.
- [Blockbench](https://github.com/ooibc88/blockbench): For the example, smart contracts (Transaction Processors) used to test the performance.
- [SEN](https://github.com/TomasTomecek/sen/tree/master/sen): Command Line Monitoring of Docker, used the CPU percentage calculation function.

## Files

The repository is composed of the following files and directories:

```

    .
    ├── benchmarks                     # Example Transaction Processors, some of them were used during the benchmark
    ├── clean.sh                       # Bash Script for stopping ALL the Docker Containers, Volumes and Networks 
    ├── dev                            # Contains development utilities 
    ├── docker                         # Contains the Dockerfiles and contexts for the deployment
    ├── docker-compose-boot-node.yaml  # Docker Compose for the First Node of the network
    ├── docker-compose-node.yaml       # Docker Compose Template for the other nodes of the network
    ├── docker_monitor.py              # Script for monitoring the docker containers
    ├── docker_starter.py              # Script for starting the nodes of the network
    ├── lib                            # Python modules used in the main scripts
    ├── logs                           # Folder for holding the logs of the main scripts
    ├── README.md                      # This file
    ├── requirements.txt               # Python packages required for running the scripts
    ├── sawtooth_client.py             # Script for sending transactions to the rest API
    └── sawtooth_subscriber.py         # Script for receiving the events from the validator

```

## Setup

This setup is meant for Ubuntu 16.04. Other distributions may be supported in the future. 
Refer to [Sawtooth Documentation](https://sawtooth.hyperledger.org/docs/core/releases/1.1.2/introduction.html) for more details.

### Getting the files

1.) Install **git**:

```bash
sudo apt install git
```

2.) Clone the repository:

```bash
git clone https://sofianBnh@bitbucket.org/sofianBnh/benchmark-starter.git
```

### Installing the Packages

1.) Installing **pip3**:

```bash
sudo apt install python3-pip
```

2.) Installing the **requirements**:

```bash
cd benchmark-starter
pip3 install -r requirements.txt
```

*Troubleshooting*: In case of the error `locale.Error: unsupported locale setting` use the following command:

```bash
export LC_ALL=C
```

### Installing Docker and Docker Compose

1.) Installing **Docker** (Ubuntu:16.04):

```bash
sudo apt -y remove docker docker-engine docker.io
sudo apt update
sudo apt -y install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository  \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable" -y
sudo apt update
sudo apt install -y docker-ce
```

2.) Adding user to group Docker:

```bash
sudo usermod -aG docker $USER
logout
```

*Troubleshooting*: In case of the error `The following packages have unmet dependencies: ...` use the following commands:

```bash
sudo add-apt-repository universe 
sudo add-apt-repository multiverse 
sudo add-apt-repository ppa:ubuntu-sdk-team/ppa
sudo apt update
sudo apt install -y docker-ce
```

For other distributions see [The official Documentation](https://docs.docker.com/)


3.) Installing **Docker Compose**:

```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

### Installing the Sawtooth SDK

Installing the **Sawtooth SDK**:

```bash
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 8AA7AF1F1091A5FD 
sudo sh -c "echo 'deb [arch=amd64] http://repo.sawtooth.me/ubuntu/bumper/stable xenial universe' >> /etc/apt/sources.list"
sudo apt update
sudo apt install python3-sawtooth-sdk -y
```

## Starting the Benchmark

For benchmarking the sawtooth network, we used a script for:

- Deploying the network.
- Sending the transactions.
- Receiving the confirmation events.
- Monitoring the usage of the containers.

### Starting the Nodes

**Purpose:**

Starting the nodes of the network using docker ad docker compose.

**Help:**

```
    usage: docker_starter.py [-h] [-v] [-c NODE_COUNT] -t NODE_TEMPLATE
                            [-b BOOT_NODE]

    optional arguments:
    -h, --help            show this help message and exit
    -v, --verbose         Increase level of output sent to stderr
    -c NODE_COUNT, --node_count NODE_COUNT
                            Number of nodes in the network
    -t NODE_TEMPLATE, --node_template NODE_TEMPLATE
                            Docker compose template of the standard nodes
    -b BOOT_NODE, --boot_node BOOT_NODE
                            Docker compose of the Boot if any
```

**Example:**

```bash
python3 docker_starter.py -c 3 -b docker-compose-boot-node.yaml -t docker-compose-node.yaml
```

This command will start the **boot node** AND **3** other nodes that are based on the template specified.

### Starting the Monitoring

**Purpose:**

Monitor the containers that are being used by the 

**Help:**

```
    usage: docker_monitor.py [-h] [-v] [-i INTERVAL] [-s SERVICES [SERVICES ...]]
                            [-b BENCHMARK]

    optional arguments:
    -h, --help            show this help message and exit
    -v, --verbose         Increase level of output sent to stderr
    -i INTERVAL, --interval INTERVAL
                            Interval of measures in seconds
    -s SERVICES [SERVICES ...], --services SERVICES [SERVICES ...]
                            Container names of the services to monitor separated by spaces
    -b BENCHMARK, --benchmark BENCHMARK
                            Name of the test
```

**Example:**

```bash
python3 docker_monitor.py -b do-nothing
```

This command will start monitoring ALL the containers that are up and will save the results in a 
file `logs/date-of-the-day-do-nothing-benchmark-results.csv`.


### Starting the Client

**Purpose:**

Send the requests to the Transaction Processors

**Help:**

```
    usage: sawtooth_client.py [-h] [-v] [-l LOAD] -b BENCHMARK [-p PUBLISHER]
                            [-s SERVER]

    optional arguments:
    -h, --help            show this help message and exit
    -v, --verbose         Increase level of output sent to stderr
    -l LOAD, --load LOAD  Number of transactions to send
    -b BENCHMARK, --benchmark BENCHMARK
                            Name of the benchmark
    -s SERVER, --server SERVER
                            IP or domain of the rest api of a server
```

**Example:**

```bash
python3 sawtooth_client.py -l 10 -s localhost -b do-nothing
```

This command will start a client which will send 10 transactions to the Rest API with the address `http://localhost:8008` 
to the transaction family **do-nothing** with version **1.0**.



### Starting the Subscriber

**Purpose:**

Listen to confirmation events from the Transaction Processors

**Help:**

```
    usage: sawtooth_subscriber.py [-h] [-v] [-l LOAD] -b BENCHMARK [-p PUBLISHER]

    optional arguments:
    -h, --help            show this help message and exit
    -v, --verbose         Increase level of output sent to stderr
    -l LOAD, --load LOAD  Number of transactions to send
    -b BENCHMARK, --benchmark BENCHMARK
                            Name of the benchmark
    -p PUBLISHER, --publisher PUBLISHER
                            IP or domain of the publisher node
```

**Example:**

```bash
python3 sawtooth_subscriber.py -p localhost -b do-nothing
```

This command will start the subscriber which will try to subscribe to the publisher with the address `http://localhost:4004`.
Then it will start listening to events with the type: **do-nothing-event**. The events count will be stored in a file
`logs/date-of-the-day-do-nothing-benchmark-events.csv`.


## Adding a Transaction Processor

To add a transaction processor based on the project's structure, one needs to do the following:

- Make the transaction processor by following the template in `dev/smart-contract-template`:

```
  .
  ├── contract
  │   ├── __init__.py
  │   ├── main.py # based on the one available in the docs  
  │   └── processor
  │       └── __init__.py # Handler Class and other modules
  └── Dockerfile # based on the one available in the docs 
```

- Create a child class of the **TransactionHandler** and import it and use it in the main.
- Add the container configuration in the *boot node compose* and the *node template compose* files as such:
  
```yaml
## In the boot node
custom-boot-node:
  container_name: custom-boot-node
  build:
    context: path/to/custom
    dockerfile: ./Dockerfile
  environment:
    validator: boot-node-validator
  depends_on:
    - "boot-node-validator"
  networks:
    - main-net
```

```yaml
## In the node template
custom-TAG:
  container_name: custom-boot-node
  build:
    context: path/to/custom
    dockerfile: ./Dockerfile
  environment:
    validator: validator-TAG
  depends_on:
    - validator-TAG
  networks:
    - NET_main-net
```