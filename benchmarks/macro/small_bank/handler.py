import hashlib
import logging

from sawtooth_sdk.processor.handler import TransactionHandler

from benchmarks.macro.small_bank.config import SMALL_BANK_NAMESPACE, SMALL_BANK_FAMILY_NAME
from benchmarks.macro.small_bank.small_bank_payload import SmallBankPayload
from benchmarks.macro.small_bank.small_bank_state import SmallBankState

LOGGER = logging.getLogger(__name__)

INITIAL_BANK = {
    "ivan": 10000000000,
    "raven": 10000000000,
}


class SmallBankTransactionHandler(TransactionHandler):
    def __init__(self):
        self._namespace_prefix = SMALL_BANK_NAMESPACE

    @property
    def family_name(self):
        return SMALL_BANK_FAMILY_NAME

    @property
    def family_versions(self):
        return ['1.0']

    @property
    def namespaces(self):
        return [self._namespace_prefix]

    def apply(self, transaction, context):
        payload = SmallBankPayload(transaction.payload)
        LOGGER.info('Executing Call to %s' % payload.action)

        state = SmallBankState(context)

        if payload.action == "check":
            balance = state.check_balance(payload.owner)
            context.add_event("{}-event".format(SMALL_BANK_FAMILY_NAME), {})
            return balance

        elif payload.action == "send":
            state.send_payment(
                payload.owner,
                payload.recipient,
                payload.amount
            )
        elif payload.action == "init":
            state.init_bank(INITIAL_BANK)

        context.add_event("{}-event".format(SMALL_BANK_FAMILY_NAME), {})
