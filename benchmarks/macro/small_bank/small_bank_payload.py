import json

from sawtooth_sdk.processor.exceptions import InvalidTransaction


class SmallBankPayload(object):

    def __init__(self, payload):
        try:
            data = json.loads(payload.decode('utf-8'))
        except ValueError:
            raise InvalidTransaction("Invalid payload serialization")

        action = data.get('action')
        owner = data.get('owner')
        amount = data.get('amount')
        recipient = data.get('recipient')

        if not action:
            raise InvalidTransaction('Action is required')

        if action not in ('check', 'send', 'init'):
            raise InvalidTransaction('Invalid action: {}'.format(action))

        if not owner and action != 'init':
            raise InvalidTransaction('Owner is required')

        if action == "send":
            if amount is None or recipient is None:
                raise InvalidTransaction('Amount and Recipient are required in a transfer')

        self._action = action
        self._owner = owner
        self._amount = amount
        self._recipient = recipient

    @property
    def action(self):
        return self._action

    @property
    def owner(self):
        return self._owner

    @property
    def amount(self):
        return self._amount

    @property
    def recipient(self):
        return self._recipient
