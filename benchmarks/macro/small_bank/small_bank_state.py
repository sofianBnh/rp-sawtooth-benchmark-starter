import hashlib
import json

from sawtooth_sdk.processor.context import Context
from sawtooth_sdk.processor.exceptions import InvalidTransaction

from benchmarks.macro.small_bank.config import SMALL_BANK_NAMESPACE


def _deserialize(data):
    return json.loads(data.decode('utf-8'))


def _serialize(data):
    return json.dumps(data, sort_keys=True).encode('utf-8')


def _make_bank_address(bank):
    return SMALL_BANK_NAMESPACE + hashlib.sha512(bank.encode('utf-8')).hexdigest()[:64]


MAIN_BANK = 'main-bank'


class SmallBankState(object):
    TIMEOUT = 3

    def __init__(self, context: Context):
        self._context = context

    def send_payment(self, owner, recipient, amount):
        owner_balance = self.check_balance(owner)
        bank = self._get_bank_state()

        if owner_balance < int(amount):
            raise InvalidTransaction('Owner {} does not have enough resources'.format(owner))

        bank[owner] -= amount
        bank[recipient] += amount

        self._update_bank_state(bank)

    def check_balance(self, owner):
        bank = self._get_bank_state()
        if owner not in bank.keys():
            raise InvalidTransaction('Owner {} is not subscribed to the bank'.format(owner))
        return bank[owner]

    def init_bank(self, record: dict):
        self._update_bank_state(record)

    def _update_bank_state(self, record):
        address = _make_bank_address(MAIN_BANK)
        data = _serialize(record)
        self._context.set_state({address: data})

    def _get_bank_state(self):
        address = _make_bank_address(MAIN_BANK)
        current_record = self._context.get_state([address])
        bank = _deserialize(current_record[0].data)
        return bank
