import hashlib
import logging

from sawtooth_sdk.processor.context import Context
from sawtooth_sdk.processor.exceptions import InvalidTransaction
from sawtooth_sdk.processor.handler import TransactionHandler

from benchmarks.micro.io_heavy.io_payload import IOPayload

LOGGER = logging.getLogger(__name__)

IO_HEAVY_FAMILY_NAME = 'io-heavy'
IO_HEAVY_NAMESPACE = hashlib.sha512(
    IO_HEAVY_FAMILY_NAME.encode('utf-8')).hexdigest()[0:6]


def _int_to_hex(integer):
    return str(hex(integer)[2:].zfill(64))


def _hex_to_int(hexadecimal):
    return int(hexadecimal, 16)


def _make_address(number):
    return IO_HEAVY_NAMESPACE + _int_to_hex(number)


def _write_all(context: Context, start_key, size):
    for i in range(size):
        address = _make_address(_int_to_hex(start_key + i))
        data = size.encode("utf-8")
        context.set_state({address: data})


def _scan_all(context: Context, start_key, size):
    for i in range(size):
        address = _make_address(_int_to_hex(start_key + i))
        _ = context.get_state([address])


class IOHeavyTransactionHandler(TransactionHandler):
    def __init__(self):
        self._namespace_prefix = IO_HEAVY_NAMESPACE

    @property
    def family_name(self):
        return IO_HEAVY_FAMILY_NAME

    @property
    def family_versions(self):
        return ['1.0']

    @property
    def namespaces(self):
        return [self._namespace_prefix]

    def apply(self, transaction, context):

        payload = IOPayload(transaction.payload)
        LOGGER.info('Executing Call to %s' % payload.action)

        if payload.action == "scan":
            _scan_all(
                context=context,
                start_key=payload.start_key,
                size=payload.size
            )
        elif payload.action == "write":
            _write_all(
                context=context,
                start_key=payload.start_key,
                size=payload.size
            )
        else:
            raise InvalidTransaction("Something went wrong")

        context.add_event("{}-event".format(IO_HEAVY_FAMILY_NAME), {})
