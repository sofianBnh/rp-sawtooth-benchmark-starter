from sawtooth_sdk.processor.exceptions import InvalidTransaction


class IOPayload(object):

    def __init__(self, payload):
        try:
            action, start_key, size = payload.decode('utf-8').split(",")
        except ValueError:
            raise InvalidTransaction("Invalid payload serialization")

        if not action:
            raise InvalidTransaction('Action is required')

        if action not in ('scan', 'write'):
            raise InvalidTransaction('Invalid action: {}'.format(action))

        if not start_key:
            raise InvalidTransaction('Start Key is required')

        if not size:
            raise InvalidTransaction('Size is required')

        self._action = action
        self._start_key = start_key
        self._size = size

    @property
    def action(self):
        return self._action

    @property
    def size(self):
        return self._size

    @property
    def start_key(self):
        return self._start_key

