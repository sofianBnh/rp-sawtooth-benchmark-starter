import hashlib
import logging

from sawtooth_sdk.processor.context import Context
from sawtooth_sdk.processor.handler import TransactionHandler

DO_NOTHING_FAMILY_NAME = 'do-nothing'
DO_NOTHING_NAMESPACE = hashlib.sha512(
    DO_NOTHING_FAMILY_NAME.encode('utf-8')).hexdigest()[0:6]

LOGGER = logging.getLogger(__name__)


class DoNothingTransactionHandler(TransactionHandler):
    def __init__(self):
        self._namespace_prefix = DO_NOTHING_NAMESPACE

    @property
    def family_name(self):
        return DO_NOTHING_FAMILY_NAME

    @property
    def family_versions(self):
        return ['1.0']

    @property
    def namespaces(self):
        return [self._namespace_prefix]

    def apply(self, transaction, context: Context):
        LOGGER.info('Executing Call to do nothing')
        context.add_event("{}-event".format(DO_NOTHING_FAMILY_NAME), {})
