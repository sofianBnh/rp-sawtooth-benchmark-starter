import hashlib
import logging

from sawtooth_sdk.processor.handler import TransactionHandler

LOGGER = logging.getLogger(__name__)

CPU_HEAVY_FAMILY_NAME = 'cpu-heavy'
CPU_HEAVY_NAMESPACE = hashlib.sha512(
    CPU_HEAVY_FAMILY_NAME.encode('utf-8')).hexdigest()[0:6]


class CPUHeavyTransactionHandler(TransactionHandler):
    def __init__(self):
        self._namespace_prefix = CPU_HEAVY_NAMESPACE

    @property
    def family_name(self):
        return CPU_HEAVY_FAMILY_NAME

    @property
    def family_versions(self):
        return ['1.0']

    @property
    def namespaces(self):
        return [self._namespace_prefix]

    def apply(self, transaction, context):
        size = int(transaction.payload.decode()) or 10
        LOGGER.info("Sorting table of %s elements" % size)
        data = []
        for i in range(size):
            data.append(i - i)

        sorted(data)
        context.add_event("{}-event".format(CPU_HEAVY_FAMILY_NAME), {})
