import hashlib
import logging

from sawtooth_sdk.processor.context import Context
from sawtooth_sdk.processor.exceptions import InvalidTransaction
from sawtooth_sdk.processor.handler import TransactionHandler

from .exception import KVStoreException
from .kv_payload import KVStorePayload

LOGGER = logging.getLogger(__name__)

KV_STORE_FAMILY_NAME = 'kv-store'
KV_STORE_NAMESPACE = hashlib.sha512(
    KV_STORE_FAMILY_NAME.encode('utf-8')).hexdigest()[0:6]


def _make_address(key):
    return KV_STORE_NAMESPACE + hashlib.sha512(
        key.encode('utf-8')).hexdigest()[-64:]


def _get_value(context: Context, key):
    address = _make_address(key)
    payload = context.get_state([address])
    if payload is None:
        raise KVStoreException("{} does not have a value".format(key))
    LOGGER.debug('decoding {}'.format(payload[0].data))
    key, value = payload[0].data.decode("utf-8").split(",")
    return value


def _put_value(context: Context, key, value):
    address = _make_address(key)
    data = "{},{}".format(key, value).encode("utf-8")
    context.set_state({address: data})


class KVStoreTransactionHandler(TransactionHandler):
    def __init__(self):
        self._namespace_prefix = KV_STORE_NAMESPACE

    @property
    def family_name(self):
        return KV_STORE_FAMILY_NAME

    @property
    def family_versions(self):
        return ['1.0']

    @property
    def namespaces(self):
        return [self._namespace_prefix]

    def apply(self, transaction, context):
        payload = KVStorePayload(transaction.payload)
        LOGGER.info('Executing Call to %s a value' % payload.action)

        try:
            if payload.action == "get":
                value = _get_value(context=context, key=payload.key)
                context.add_event("{}-event".format(KV_STORE_FAMILY_NAME), {})
                return value

            elif payload.action == "put":
                _put_value(
                    context=context,
                    key=payload.key,
                    value=payload.value
                )
                context.add_event("{}-event".format(KV_STORE_FAMILY_NAME), {})

            else:
                raise KVStoreException("Something went wrong")
        except KVStoreException as err:
            raise InvalidTransaction(str(err))
