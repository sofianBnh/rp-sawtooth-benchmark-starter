from sawtooth_sdk.processor.exceptions import InvalidTransaction


class KVStorePayload(object):

    def __init__(self, payload):
        try:
            data = payload.decode('utf-8').split(",")

            if len(data) == 2:
                action, key = data
                value = None
            elif len(data) == 3:
                action, key, value = data
            else:
                raise InvalidTransaction("Invalid payload, Not enough arguments")

        except ValueError:
            raise InvalidTransaction("Invalid payload serialization")

        if not action:
            raise InvalidTransaction('Action is required')
        if action not in ('get', 'put'):
            raise InvalidTransaction('Invalid action: {}'.format(action))

        if not key:
            raise InvalidTransaction('Key is required')

        if action == 'put':
            if not value:
                raise InvalidTransaction(
                    'Value is required for "put" transaction')

        self._action = action
        self._key = key
        self._value = value

    @property
    def action(self):
        return self._action

    @property
    def value(self):
        return self._value

    @property
    def key(self):
        return self._key
