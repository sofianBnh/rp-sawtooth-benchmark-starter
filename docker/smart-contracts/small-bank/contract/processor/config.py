import hashlib

SMALL_BANK_FAMILY_NAME = 'small-bank'
SMALL_BANK_NAMESPACE = hashlib.sha512(
    SMALL_BANK_FAMILY_NAME.encode('utf-8')).hexdigest()[0:6]
